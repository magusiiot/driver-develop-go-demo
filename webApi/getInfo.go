package webApi

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/beego/beego/v2/core/logs"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
)

type AppInfoResponse struct {
	Code  int        `json:"code"`
	Count int        `json:"count"`
	Data  []AppInfoS `json:"data"`
	Msg   string     `json:"msg"`
}

type AppInfoS struct {
	Desc       string `json:"desc"`
	DriverType string `json:"driverType"`
	Host       string `json:"host"`
	Id         int    `json:"id"`
	Name       string `json:"name"`
	Port       int    `json:"port"`
	Status     string `json:"status"`
	Type       string `json:"type"`
}

type DriverConfResponse struct {
	Code  int `json:"code"`
	Count int `json:"count"`
	Data  []struct {
		Id       int    `json:"id"`
		Driver   string `json:"driver"`
		Model    string `json:"model"`
		ExScope  string `json:"exScope"`
		Name     string `json:"name"`
		Key      string `json:"key"`
		Value    string `json:"value"`
		Ed       string `json:"ed"`
		Flag     string `json:"flag"`
		TextType string `json:"textType"`
		TextData string `json:"textData"`
		Data1    string `json:"data1"`
		Data2    string `json:"data2"`
	} `json:"data"`
	Msg string `json:"msg"`
}

type PointResponse struct {
	Code  int      `json:"code"`
	Count int      `json:"count"`
	Data  []*Point `json:"data"`
	Msg   string   `json:"msg"`
}

type Point struct {
	ID      int     `json:"ID"`
	UID     int     `json:"UID"`
	PID     int     `json:"PID"`
	EVENT   int     `json:"EVENT"`
	PN      string  `json:"PN"`
	GN      string  `json:"GN"`
	SN      string  `json:"SN"`
	RT      int     `json:"RT"`
	AN      string  `json:"AN"`
	KR      string  `json:"KR"`
	SR      string  `json:"SR"`
	FB      int     `json:"FB"`
	FK      int     `json:"FK"`
	CP      string  `json:"CP"`
	AD      string  `json:"AD"`
	EU      string  `json:"EU"`
	TV      int     `json:"TV"`
	BV      int     `json:"BV"`
	DT      int     `json:"DT"`
	DB      float64 `json:"DB"`
	KZ      int     `json:"KZ"`
	EX      string  `json:"EX"`
	DS      int     `json:"DS"`
	PH      int     `json:"PH"`
	PL      int     `json:"PL"`
	ED      string  `json:"ED"`
	FQ      int     `json:"FQ"`
	FM      int     `json:"FM"`
	PT      int     `json:"PT"`
	KT      int     `json:"KT"`
	AP      int     `json:"AP"`
	LC      int     `json:"LC"`
	H4      int     `json:"H4"`
	H3      int     `json:"H3"`
	ZH      int     `json:"ZH"`
	HL      int     `json:"HL"`
	LL      int     `json:"LL"`
	ZL      int     `json:"ZL"`
	L3      int     `json:"L3"`
	L4      int     `json:"L4"`
	C1      int     `json:"C1"`
	C2      int     `json:"C2"`
	C3      int     `json:"C3"`
	C4      int     `json:"C4"`
	C5      int     `json:"C5"`
	C6      int     `json:"C6"`
	C7      int     `json:"C7"`
	C8      int     `json:"C8"`
	AV      int     `json:"AV"`
	AS      string  `json:"AS"`
	TM      int     `json:"TM"`
	FLAG    string  `json:"FLAG"`
	SYNC    string  `json:"SYNC"`
	REDATA1 string  `json:"REDATA1"`
	REDATA2 string  `json:"REDATA2"`
	TA      int     `json:"TA"`
	TF      int     `json:"TF"`
	MAXV    int     `json:"MAXV"`
	MINV    int     `json:"MINV"`
	MAXTIME int     `json:"MAXTIME"`
	MINTIME int     `json:"MINTIME"`
	AVGV    int     `json:"AVGV"`
	FLOW    int     `json:"FLOW"`
	PS      int     `json:"PS"`
	DD      string  `json:"DD"`
	KS      int     `json:"KS"`
	PD      int     `json:"PD"`
	AU      string  `json:"AU"`
	UD      int     `json:"UD"`
	HI      string  `json:"HI"`
	CT      int     `json:"CT"`
	TI      int     `json:"TI"`
	FS      int     `json:"FS"`
	DL      int     `json:"DL"`
	CD      int     `json:"CD"`
	IV      int     `json:"IV"`
	TP      int     `json:"TP"`
	ND      int     `json:"ND"`
	OT      int     `json:"OT"`
	TT      int     `json:"TT"`
	SG      string  `json:"SG"`
	AR      int     `json:"AR"`
	HW      int     `json:"HW"`
	ST      string  `json:"ST"`
	OF      int     `json:"OF"`
	BP      int     `json:"BP"`
	FL      int     `json:"FL"`
	RS      string  `json:"RS"`
	TN      string  `json:"TN"`
	Count   int     `json:"Count"`
}

var AppInfo AppInfoS
var Points []*Point
var DriverConf DriverConfResponse
var SyncPoints sync.Map

type Config struct {
	Key   string
	Value string
}

var Config_IsLink Config                // 测点添加模式，true 为自动添加到实时库;false 为手动添加到实时库
var Config_ReceiveDataIpAddress Config  // 接收数据IP地址
var Config_ReceiveDataPortNumber Config // 接收数据端口号
var Config_LocalDataIpAddress Config    // 本地Ip地址
var Config_LocalDataPortNumber Config   // 本地端口号
var Config_MaximumMessageLength Config  // 报文最大长度
var Config_SourceInterval Config        // 数据的读取间隔,循环读取一次的间隔时间,单位毫秒
var Config_AppLogsLevel Config          // 程序记录的日志级别 Info|Notice|Warning|Error|Alert|Emergency|Debug
var Config_ControlEnable Config         // 是否开启服务器控制功能, false: 不开启服务器控制 true: 开启服务器控制
var Config_ControlAddress Config        // 下发控制命令的服务器, 控制到采集之间不能有隔离器
var Config_ControlUserName Config       // 下发控制命令的服务器用户名
var Config_ControlUserPassword Config   // 下发控制命令的服务器密码
var Config_ControlNamePrefix Config     // 控制的节点名
var Config_WebListenAddress Config      // 提供WEB服务的地址

//var DassUrl = "http://127.0.0.1:8085"
var DassUrl = "http://192.168.40.43:8085"

func init() {
	Config_IsLink.Key = "isLink"
	Config_ReceiveDataIpAddress.Key = "receive_data_ip_address"
	Config_ReceiveDataPortNumber.Key = "receive_data_port_number"
	Config_LocalDataIpAddress.Key = "local_data_ip_address"
	Config_LocalDataPortNumber.Key = "local_data_port_number"
	Config_MaximumMessageLength.Key = "maximum_message_length"
	Config_SourceInterval.Key = "source_interval"
	Config_AppLogsLevel.Key = "app_logs_level"
	Config_ControlEnable.Key = "control_enable"
	Config_ControlAddress.Key = "control_address"
	Config_ControlUserName.Key = "control_user_name"
	Config_ControlUserPassword.Key = "control_user_password"
	Config_ControlNamePrefix.Key = "control_name_prefix"
	Config_WebListenAddress.Key = "web_listen_address"
}

func GetConfAndPoints() {
	err := GetAppInfo()
	if err != nil {
		logs.Warn("cannot connect dass:", err)
		return
	} else {
		GetConf()
		GetPoint()
	}
}

// GetBinName 获取程序文件名
func GetBinName() string {
	return strings.TrimSuffix(filepath.Base(os.Args[0]), path.Ext(os.Args[0]))
}

// GetAppInfo 获取驱动信息
func GetAppInfo() error {
	BinName := GetBinName()
	url := DassUrl + "/v1/das/app?desc=" + BinName
	//url := DassUrl + "/v1/das/app?desc=" + "test1"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		logs.Warn(err)
		return err
	}
	res, err := client.Do(req)
	if err != nil {
		logs.Warn(err)
		return err
	}
	defer res.Body.Close()

	//fmt.Println("res.Status:",res.Status)
	//if res.StatusCode != 200 {
	//	return fmt.Errorf("time out")
	//}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		logs.Warn(err)
		return err
	}
	appRes := AppInfoResponse{}
	err = json.Unmarshal(body, &appRes)
	if err != nil {
		logs.Warn("Unmarshal err:", err)
	}
	if len(appRes.Data) > 0 {
		AppInfo = appRes.Data[0]
	} else {
		fmt.Errorf("获取驱动信息失败")
	}
	logs.Debug("GetAppInfo:", string(body))
	return err
}

// GetConf 获取驱动配置信息
func GetConf() error {
	url := DassUrl + "/v1/das/conf/" + strconv.Itoa(AppInfo.Id)
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		logs.Warn(err)
		return err
	}
	res, err := client.Do(req)
	if err != nil {
		logs.Warn(err)
		return err
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		logs.Warn(err)
		return err
	}

	err = json.Unmarshal(body, &DriverConf)
	if err != nil {
		logs.Warn("Unmarshal err:", err)
	}

	if len(DriverConf.Data) > 0 {
		for _, datum := range DriverConf.Data {
			if datum.Key == Config_IsLink.Key {
				Config_IsLink.Value = datum.Value
			}
			if datum.Key == Config_ReceiveDataIpAddress.Key {
				Config_ReceiveDataIpAddress.Value = datum.Value
			}
			if datum.Key == Config_ReceiveDataPortNumber.Key {
				Config_ReceiveDataPortNumber.Value = datum.Value
			}
			if datum.Key == Config_LocalDataIpAddress.Key {
				Config_LocalDataIpAddress.Value = datum.Value
			}
			if datum.Key == Config_LocalDataPortNumber.Key {
				Config_LocalDataPortNumber.Value = datum.Value
			}
			if datum.Key == Config_MaximumMessageLength.Key {
				Config_MaximumMessageLength.Value = datum.Value
			}
			if datum.Key == Config_SourceInterval.Key {
				Config_SourceInterval.Value = datum.Value
			}
			if datum.Key == Config_AppLogsLevel.Key {
				if datum.Value == "emergency" {
					Config_AppLogsLevel.Value = "0"
				}
				if datum.Value == "alert" {
					Config_AppLogsLevel.Value = "1"
				}
				if datum.Value == "critical" {
					Config_AppLogsLevel.Value = "2"
				}
				if datum.Value == "error" {
					Config_AppLogsLevel.Value = "3"
				}
				if datum.Value == "warning" {
					Config_AppLogsLevel.Value = "4"
				}
				if datum.Value == "notice" {
					Config_AppLogsLevel.Value = "5"
				}
				if datum.Value == "info" {
					Config_AppLogsLevel.Value = "6"
				}
				if datum.Value == "debug" {
					Config_AppLogsLevel.Value = "7"
				}

			}
			if datum.Key == Config_ControlEnable.Key {
				Config_ControlEnable.Value = datum.Value
			}
			if datum.Key == Config_ControlAddress.Key {
				Config_ControlAddress.Value = datum.Value
			}
			if datum.Key == Config_ControlUserName.Key {
				Config_ControlUserName.Value = datum.Value
			}
			if datum.Key == Config_ControlUserPassword.Key {
				if datum.TextType == "ispassword" {
					Config_ControlUserPassword.Value, _ = DecodePasswd(datum.Value)
				} else {
					Config_ControlUserPassword.Value = datum.Value
				}
			}
			if datum.Key == Config_ControlNamePrefix.Key {
				Config_ControlNamePrefix.Value = datum.Value
			}
			if datum.Key == Config_WebListenAddress.Key {
				Config_WebListenAddress.Value = datum.Value
			}
		}
	}

	logs.Debug("GetConf:", string(body))
	return err
}

func GetPoint() {
	url := DassUrl + "/v1/point/?SN=" + AppInfo.Name + "&Limit=1000000"
	logs.Info("GetPoint url:", url)
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		logs.Warn(err)
		return
	}
	res, err := client.Do(req)
	if err != nil {
		logs.Warn(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		logs.Warn(err)
		return
	}

	points := PointResponse{Data: Points}
	err = json.Unmarshal(body, &points)
	if err != nil {
		logs.Warn("Unmarshal err:", err)
	}
	for _, point := range points.Data {
		if point.AD != "" {
			SyncPoints.Store(point.UID, point.AD)
		} else {
			SyncPoints.Delete(point.UID)
		}
	}

	logs.Debug("GetPoint:", string(body))
	return
}

func DecodePasswd(pwd string) (string, error) {
	decodeString, err := base64.StdEncoding.DecodeString(pwd)
	if err != nil {
		return "", err
	}
	decrypt, err := AesDecrypt(decodeString, []byte("0f90023fc9ae101e"))
	if err != nil {
		return "", err
	}
	return string(decrypt), nil
}

//AES解密
func AesDecrypt(crypted, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, key[:blockSize])
	origData := make([]byte, len(crypted))
	blockMode.CryptBlocks(origData, crypted)
	origData = PKCS7UnPadding(origData)
	logs.Debug("解密密码为：", string(origData))
	return origData, nil

}
func PKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}
