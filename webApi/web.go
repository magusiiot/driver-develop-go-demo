package webApi

import (
	"encoding/json"
	"github.com/beego/beego/v2/core/logs"
	"net/http"
	"os"
	"time"
)

// TodoCommand 提供web服务接口
func TodoCommand(res http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	defer req.Body.Close()
	comm := req.FormValue("command")
	op := req.FormValue("op")
	logs.Info("command:", comm)
	//停止驱动
	if "Stop" == comm {
		res.Write([]byte("success"))
		time.Sleep(time.Second * 1)
		os.Exit(0)
	}
	//更新测点数据
	if "notify" == comm && op == "update" {
		GetPoint()
	}
	//状态查询
	if "Status" == comm {
		a := map[string]string{
			"name":  "提供WEB服务的地址",
			"key":   Config_WebListenAddress.Key,
			"value": Config_WebListenAddress.Value,
		}
		r, err := json.Marshal(a)
		if err != nil {
			logs.Warn("json.Marshal(a) err:", err)
		}
		res.Write(r)
	}
}
