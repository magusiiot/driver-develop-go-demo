module gitee.com/magusiiot/driver-develop-go-demo

go 1.16

require (
	github.com/beego/beego/v2 v2.0.1
	github.com/gocarina/gocsv v0.0.0-20210516172204-ca9e8a8ddea8
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/ini.v1 v1.62.0
)
