package main

import (
	"fmt"
	"gitee.com/magusiiot/driver-develop-go-demo/openplant"
	"gitee.com/magusiiot/driver-develop-go-demo/receive"
	"gitee.com/magusiiot/driver-develop-go-demo/webApi"
	"github.com/beego/beego/v2/core/logs"
	"net/http"
	"strconv"
)

func init() {
	setLog()
}

func main() {

	// 获取报文数据，不同的报文获取方式不同，需要自己实现
	go receive.ReceiveMsg()
	//将数据写入
	go openplant.WirteRealtime()
	//开启http服务
	mux := http.NewServeMux()
	mux.HandleFunc("/todo", webApi.TodoCommand)
	addr := "0.0.0.0:"
	if webApi.AppInfo.Port != 0 {
		addr += strconv.Itoa(webApi.AppInfo.Port)
	} else {
		addr += "0.0.0.0:8889"
	}
	server := &http.Server{
		Addr:    addr,
		Handler: mux,
	}
	logs.Info("listen addr:", addr)
	server.ListenAndServe()
}

func setLog() {
	logs.EnableFuncCallDepth(true)
	logs.SetLogFuncCallDepth(4)
	logs.SetLogger(logs.AdapterFile, fmt.Sprintf(`{"filename":"%s.log","level":%s,"maxlines":0,"maxsize":0,"daily":true,"maxdays":10,"color":true}`, webApi.GetBinName(), webApi.Config_AppLogsLevel.Value))
}
