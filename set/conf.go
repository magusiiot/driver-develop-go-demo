package set

import (
	"encoding/csv"
	"github.com/beego/beego/v2/core/logs"
	"github.com/gocarina/gocsv"
	"gopkg.in/ini.v1"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"
)

type Config struct {
	Id       string `csv:"Id"`
	Desc     string `csv:"Desc"`
	Scope    string `csv:"Scope"`
	Tips     string `csv:"Tips"`
	Default  string `csv:"Default"`
	TextType string `csv:"TextType"`
	TextData string `csv:"TextData"`
	Validate string `csv:"Validate"`
}

var SourceAddress Config
var Id Config
var Pn Config
var Rt Config
var Ad Config
var Ed Config
var Fk Config
var Fb Config
var AppLogsLevel Config
var DestinationAddress Config
var DestinationUserName Config
var DestinationUserPassword Config
var DestinationOption Config
var WebListenAddress Config
var RpcListenAddress Config

var Version string
var Description string
var DriverType string
var PublishTime string
var UploadTime string
var Name string
var Platform string
var Industry string
var Path string
var valid bool
var delay int

var BinName string
var Port string

func init() {
	BinName = strings.TrimSuffix(filepath.Base(os.Args[0]), path.Ext(os.Args[0]))
}

func ReadConfig(path string) {
	configFile, err := os.OpenFile(path, os.O_RDWR, os.ModePerm)
	if err != nil {
		panic(err)
	}
	gocsv.SetCSVReader(func(in io.Reader) gocsv.CSVReader {
		r := csv.NewReader(in)
		r.Comma = '\t'
		return r
	})
	defer configFile.Close()
	var configs []Config
	if err := gocsv.UnmarshalFile(configFile, &configs); err != nil { // Load clients from file
		logs.Warn("gocsv.UnmarshalFile err:", err)
	}
	for _, conf := range configs {
		logs.Info(conf.Id, " ", conf.Desc, " ", conf.Scope, " ", conf.Tips, " ", conf.Default, " ", conf.TextType, " ", conf.TextData, " ", conf.Validate)
		if conf.Id == "source_address" {
			SourceAddress = conf
		}
		if conf.Id == "Id" {
			Id = conf
		}
		if conf.Id == "Pn" {
			Pn = conf
		}
		if conf.Id == "Ad" {
			Ad = conf
		}
		if conf.Id == "Ed" {
			Ed = conf
		}
		if conf.Id == "Fk" {
			Fk = conf
		}
		if conf.Id == "Fb" {
			Fb = conf
		}
		if conf.Id == "app_logs_level" {
			AppLogsLevel = conf
		}
		if conf.Id == "destination_address" {
			DestinationAddress = conf
		}
		if conf.Id == "destination_user_name" {
			DestinationUserName = conf
		}
		if conf.Id == "destination_user_password" {
			DestinationUserPassword = conf
		}
		if conf.Id == "destination_option" {
			DestinationOption = conf
		}
		if conf.Id == "web_listen_address" {
			WebListenAddress = conf
		}
		if conf.Id == "rpc_listen_address" {
			RpcListenAddress = conf
		}
	}

	/*fmt.Println("------------------------------------------------------------------------------------------")
	file, err := os.Open(path)
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}
	defer file.Close()
	//reader := csv.NewReader(transform.NewReader(file, simplifiedchinese.GBK.NewDecoder()))

	reader := csv.NewReader(file)
	reader.Comma='\t'
	reader.FieldsPerRecord = -1

	record, err := reader.ReadAll()
	if err != nil {
		panic(err)
	}
	rc := map[string]int{}
	for i, rows := range record {
		//for j, row := range rows {
		//	fmt.Printf("%s:%d\t", row,j)
		//}
		//fmt.Println()

		if i == 0 {
			for j, row := range rows {
				fmt.Printf("%s:%d\t", row,j)
				rc[row] = j
			}

			fmt.Println()
		} else {
			for j, row := range rows {
				fmt.Printf("%s:%d\t", row,j)
			}
			fmt.Println()
			if rows[rc["Id"]] == "source_address" {
				SourceAddress = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
				fmt.Println("SourceAddress:",SourceAddress)
			}
			if rows[rc["Id"]] == "Id" {
				Id = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "Pn" {
				Pn = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "Rt" {
				Rt = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "Ad" {
				Ad = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "Ed" {
				Ed = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "Fk" {
				Fk = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "Fb" {
				Fb = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "app_logs_level" {
				AppLogsLevel = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "destination_address" {
				DestinationAddress = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "destination_user_name" {
				DestinationUserName = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "destination_user_password" {
				DestinationUserPassword = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "destination_option" {
				DestinationOption = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "web_listen_address" {
				WebListenAddress = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}
			if rows[rc["Id"]] == "rpc_listen_address" {
				RpcListenAddress = Config{
					Id:       rows[rc["Id"]],
					Desc:     rows[rc["Desc"]],
					Scope:    rows[rc["Scope"]],
					Tips:     rows[rc["Tips"]],
					Default:  rows[rc["Default"]],
					TextType: rows[rc["TextType"]],
					TextData: rows[rc["TextData"]],
					Validate: rows[rc["Validate"]],
				}
			}

		}
	}*/
}

func ReadVersion(path string) {
	cfg, err := ini.Load(path)
	if err != nil {
		logs.Warn("Fail to read file: %v", err)
		os.Exit(1)
	}
	Version = cfg.Section("").Key("version").String()
	Description = cfg.Section("").Key("description").String()
	DriverType = cfg.Section("").Key("type").String()
	PublishTime = cfg.Section("").Key("publish_time").String()
	UploadTime = cfg.Section("").Key("upload_time").String()
	Name = cfg.Section("").Key("name").String()
	Platform = cfg.Section("").Key("platform").String()
	Industry = cfg.Section("").Key("industry").String()
	Path = cfg.Section("").Key("path").String()
	valid, _ = cfg.Section("").Key("valid").Bool()
	delay, _ = cfg.Section("").Key("delay").Int()

	logs.Info(Version)
	logs.Info(Description)
	logs.Info(DriverType)
	logs.Info(PublishTime)
	logs.Info(UploadTime)
	logs.Info(Name)
	logs.Info(Platform)
	logs.Info(Industry)
	logs.Info(Path)
	logs.Info(valid)
	logs.Info(delay)

}
