package openplant

import (
	"gitee.com/magusiiot/driver-develop-go-demo/webApi"
	"github.com/beego/beego/v2/core/logs"
	"strconv"
	"time"
)

// WriteChan 实时值chan
var WriteChan chan []*Realtime

// MsgChan 报文chan，将解析到的报文写入
var MsgChan chan map[int][2]interface{}

func init() {
	WriteChan = make(chan []*Realtime, 100)
	MsgChan = make(chan map[int][2]interface{}, 1000)
}

// PushMsgChan 将消息推送至chan
func PushMsgChan(msg map[int][2]interface{}) {
	MsgChan <- msg
}

// WirteRealtime 写openplant接口
func WirteRealtime() {
	for true {
		select {
		//数据处理
		case msg := <-MsgChan:
			logs.Debug("receive.MsgChan:", msg)
			points := make([]*Realtime, 0)
			webApi.SyncPoints.Range(func(key, value interface{}) bool {
				AD, _ := strconv.Atoi(value.(string))
				if value != nil {
					tmp := Realtime{
						AV: msg[AD][0],
						DS: 0,
						GN: "",
						ID: key.(int),
						//TM: int64(msg[AD][1].(int)),
					}
					logs.Debug("AV:", tmp.AV, "TM:", time.Unix(tmp.TM, 0).Format("2006-01-02 15:04:05"))
					points = append(points, &tmp)
				}
				if len(points) == 100 {
					WriteChan <- points
					points = make([]*Realtime, 0)

				}
				return true
			})

			if len(points) < 100 && len(points) > 0 {
				WriteChan <- points
			}
		case p := <-WriteChan:
			//将数据写入openplant
			WriteRealtime(p, TokenData.Data)

		default:
			//token保活
			if time.Now().Unix() > TokenData.Timeout-300 {
				KeepTokenAlive()
			}
		}

	}
}
