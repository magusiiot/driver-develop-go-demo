package openplant

import (
	"encoding/json"
	"fmt"
	"gitee.com/magusiiot/driver-develop-go-demo/webApi"
	"github.com/beego/beego/v2/core/logs"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Realtime struct {
	AV interface{} `json:"AV"`
	DS int         `json:"DS"`
	GN string      `json:"GN"`
	ID int         `json:"ID"`
	TM int64       `json:"TM"`
}

const weburl = "http://192.168.40.43:81"
const Exp = 30

type Token struct {
	Data    string
	Timeout int64
}

var TokenData Token
var mu sync.Mutex

type LoginResponse struct {
	Flag    int    `json:"flag"`
	Message string `json:"message"`
	Data    string `json:"data"`
}

func init() {

	TokenData = Token{}
	webApi.GetConfAndPoints()
	KeepTokenAlive()
}

// KeepTokenAlive 更新token
func KeepTokenAlive() {
	token, err := LoginOpenplant(webApi.Config_ControlUserName.Value, webApi.Config_ControlUserPassword.Value)
	if err != nil {
		logs.Warn("get login token err:", err)
	}
	mu.Lock()
	TokenData.Data = token
	TokenData.Timeout = time.Now().Unix() + Exp*60
	mu.Unlock()
}

// LoginOpenplant 登陆webapi
func LoginOpenplant(user, password string) (token string, err error) {
	url := weburl + "/login?user=" + user + "&password=" + password + "&period=" + strconv.Itoa(Exp)
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		logs.Warn(err)
		return
	}
	res, err := client.Do(req)
	if err != nil {
		logs.Warn(err)
		return
	}
	defer res.Body.Close()

	T := LoginResponse{}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		logs.Warn(err)
		return
	}
	logs.Debug(string(body))
	err = json.Unmarshal(body, &T)
	if err != nil {
		return "", err
	} else {
		if T.Message == "OK" {
			token = T.Data
			return T.Data, nil
		} else {
			return "", fmt.Errorf("%s", T.Message)
		}
	}
}

// WriteRealtime 写实时数据
func WriteRealtime(point []*Realtime, token string) (err error) {
	url := weburl + "/insertDataById"
	method := "POST"
	var rows string
	for _, realtime := range point {
		if realtime.ID > 0 {
			var av string
			if realtime.AV != nil {
				switch reflect.TypeOf(realtime.AV).Kind() {
				case reflect.Float32, reflect.Float64:
					av = fmt.Sprintf("%f", realtime.AV)
				case reflect.String:
					av = realtime.AV.(string)
				case reflect.Int16, reflect.Int, reflect.Int32, reflect.Int8, reflect.Int64:
					av = fmt.Sprintf("%d", realtime.AV)
				}
				rows += fmt.Sprintf("&rows=%d,%s", realtime.ID, av)
			}
		}
	}
	logs.Debug("rows:", rows)
	payload := strings.NewReader("tableName=Realtime&field=ID,AV" + rows)

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		logs.Warn(err)
		return
	}
	req.Header.Add("Authorization", token)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		logs.Warn(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		logs.Warn(err)
		return
	}
	logs.Debug(string(body))
	T := LoginResponse{}
	err = json.Unmarshal(body, &T)

	if err != nil {
		return err
	} else {
		if T.Message == "OK" {
			return nil
		} else {
			if T.Message == "Token非法" {
				KeepTokenAlive()
			}
			return fmt.Errorf("%s", T.Message)
		}
	}
}

// WriteArchive 写历史数据
func WriteArchive(point []*Realtime, token string) (err error) {
	url := weburl + "/insertDataById"
	method := "POST"
	var rows string
	for _, realtime := range point {
		if realtime.ID > 0 {
			var av string
			if realtime.AV != nil {
				switch reflect.TypeOf(realtime.AV).Kind() {
				case reflect.Float32, reflect.Float64:
					av = fmt.Sprintf("%f", realtime.AV)
				case reflect.String:
					av = realtime.AV.(string)
				case reflect.Int16, reflect.Int, reflect.Int32, reflect.Int8, reflect.Int64:
					av = fmt.Sprintf("%d", realtime.AV)
				}
				rows += fmt.Sprintf("&rows=%d,%s,%d", realtime.ID, av, realtime.TM)
			}
		}
	}
	logs.Debug("rows:", rows)
	payload := strings.NewReader("tableName=Archive&field=ID,AV,TM" + rows)

	client := &http.Client{}
	req, err := http.NewRequest(method, url, payload)

	if err != nil {
		logs.Warn(err)
		return
	}
	req.Header.Add("Authorization", token)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := client.Do(req)
	if err != nil {
		logs.Warn(err)
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		logs.Warn(err)
		return
	}
	logs.Debug(string(body))
	T := LoginResponse{}
	err = json.Unmarshal(body, &T)

	if err != nil {
		return err
	} else {
		if T.Message == "OK" {
			return nil
		} else {
			return fmt.Errorf("%s", T.Message)
		}
	}
}
