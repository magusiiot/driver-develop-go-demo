package SendService

import (
	"gitee.com/magusiiot/driver-develop-go-demo/send/byteschange"
	"github.com/beego/beego/v2/core/logs"
	"net"
)

type Config struct {
	Ip       [4]byte
	Port     int
	TimeWait int
}

type SendMsg struct {
	Head            []byte
	Start           []byte
	Length          []byte
	End             []byte
	MsgMap          map[int][2][]byte
	Count           int
	ControlLock     int
	InitialPosition int
	Min             int
}

type SendController struct {
	Config  Config
	SendMsg SendMsg
	Socket  *net.UDPConn
	Msg     []byte
	Err     error
}

func (s *SendController) Initialize() {
	s.UdpCon()
	s.SendMsg.Head = byteschange.IntToBytes(10000)
	s.SendMsg.End = byteschange.IntToBytes(10001)
	s.SendMsg.ControlLock = 0
	logs.Info("初始化完成")
}

func (s *SendController) UdpCon() {
	// 创建连接
	logs.Info("创建监听:", s.Config.Ip[0], ".", s.Config.Ip[1], ".", s.Config.Ip[2], ".", s.Config.Ip[3], ":", s.Config.Port)
	s.Socket, s.Err = net.DialUDP("udp4", nil, &net.UDPAddr{
		IP:   net.IPv4(s.Config.Ip[0], s.Config.Ip[1], s.Config.Ip[2], s.Config.Ip[3]),
		Port: s.Config.Port,
	})
	if s.Err != nil {
		logs.Warn("连接失败!", s.Err)
	}
}

func (s *SendController) Send(data []byte) {
	s.UdpCon()
	s.Msg = data
	tmp, err := s.Socket.Write(s.Msg)
	if err != nil {
		logs.Error("errors:", err)
		return
	}
	logs.Info("Msg Length:", tmp)
	logs.Info("% 2x", s.Msg)
	s.Socket.Close()
}
