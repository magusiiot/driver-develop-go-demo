package main

import (
	"gitee.com/magusiiot/driver-develop-go-demo/send/SendService"
	"gitee.com/magusiiot/driver-develop-go-demo/send/byteschange"
	"time"
)


// 发包测试程序
func main() {
	send:=SendService.SendController{
		Config: SendService.Config{
			Ip:[4]byte{0,0,0,0},
			Port: 9999,
		},
	}
	send.Initialize()

	//data:=[]byte{0x27,0x10,0x64,0xd0,0x00,0x18,
	//	0x42,0x3c,0x7e,0xdd,0x61,0x31,0xe4,0xb0,
	//	0x42,0x3c,0x7e,0xdd,0x61,0x31,0xe4,0xb0,
	//	0x42,0x3c,0x7e,0xdd,0x61,0x31,0xe4,0xb0,
	//	0x27,0x11}


	for  {
		tm := byteschange.IntToBytes(int(time.Now().Unix()))
		d1 := byteschange.IntToBytes(int(time.Now().Unix())%100+100)
		d2 := byteschange.IntToBytes(int(time.Now().Unix())%100+200)
		d3 := byteschange.IntToBytes(int(time.Now().Unix())%100+300)
		data := append(append(append(append(append(append(append([]byte{0x27,0x10,0x64,0xd0,0x00,0x18},d1...),tm...),d2...),tm...),d3...),tm...),0x27,0x11)
		send.Send(data)
		time.Sleep(1*time.Second)
	}
}
