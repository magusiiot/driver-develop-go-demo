package receive

import (
	"gitee.com/magusiiot/driver-develop-go-demo/openplant"
	"gitee.com/magusiiot/driver-develop-go-demo/receive/Udp"
	"gitee.com/magusiiot/driver-develop-go-demo/webApi"
	"github.com/beego/beego/v2/core/logs"
	"strconv"
)

func ReceiveMsg() {
	// 报文监听端口
	port, err := strconv.Atoi(webApi.Config_LocalDataPortNumber.Value)
	if err != nil || port == 0 {
		logs.Warn("get listen port err,please check config", err)
		//return
		port = 9999
	}
	//报文长度
	maxLen, err := strconv.Atoi(webApi.Config_MaximumMessageLength.Value)
	if err != nil || maxLen == 0 {
		maxLen = 4096
	}

	udp := Udp.Contoller{
		Config: Udp.Config{
			Ip:        [4]byte{127, 0, 0, 1},
			Port:      port,
			MaxLength: int32(maxLen),
		},
	}
	//初始化报文监听网络
	udp.Initialize()

	for {
		//获取报文，并解析
		udp.GetData()
		//将报文写入到chan。写入的报文需要跟chan的格式相同
		openplant.PushMsgChan(udp.DataMap)
		logs.Debug("dataMap:", udp.DataMap)
	}
}
