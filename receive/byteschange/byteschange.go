package byteschange

import (
	"bytes"
	"encoding/binary"
	"github.com/beego/beego/v2/core/logs"
	"math"
)

func IntToBytes(n int) []byte {
	data := int64(n)
	bytebuf := bytes.NewBuffer([]byte{})
	binary.Write(bytebuf, binary.BigEndian, data)
	return bytebuf.Bytes()
}

func BytesToInt16(bys []byte) int {
	var pi int16
	buf := bytes.NewReader(bys)
	err := binary.Read(buf, binary.BigEndian, &pi)
	if err != nil {
		logs.Debug("binary.Read int32 failed:", err)
		return 0
	}
	return int(pi)
}
func BytesToInt32(bys []byte) int {
	var pi int32
	buf := bytes.NewReader(bys)
	err := binary.Read(buf, binary.BigEndian, &pi)
	if err != nil {
		logs.Debug("binary.Read int32 failed:", err)
		return 0
	}
	return int(pi)
}

func Float32ToByte(float float32) []byte {
	bits := math.Float32bits(float)
	bytes := make([]byte, 4)
	binary.BigEndian.PutUint32(bytes, bits)
	return bytes
}

func ByteToFloat32(byt []byte) float32 {
	var pi float32
	buf := bytes.NewReader(byt)
	err := binary.Read(buf, binary.BigEndian, &pi)
	if err != nil {
		logs.Debug("binary.Read float32 failed:", err)
	}
	return pi
}

func Float64ToByte(float float64) []byte {
	bits := math.Float64bits(float)
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, bits)

	return bytes
}

func ByteToFloat64(byt []byte) float64 {
	var pi float64
	buf := bytes.NewReader(byt)
	err := binary.Read(buf, binary.BigEndian, &pi)
	if err != nil {
		logs.Debug("binary.Read float32 failed:", err)
	}
	return pi
}
