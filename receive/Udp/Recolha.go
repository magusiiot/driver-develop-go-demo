package Udp

import (
	"fmt"
	"gitee.com/magusiiot/driver-develop-go-demo/receive/byteschange"
	"github.com/beego/beego/v2/core/logs"
	"net"
	"sync"
)

type Config struct {
	Ip        [4]byte
	Port      int
	LIp       [4]byte
	LPort     int
	MaxLength int32
	AllLength int32
}
type Contoller struct {
	Config       Config
	UdpSocket    *net.UDPConn
	Error        error
	bHistoryFlag bool
	Data         []byte
	DataMap      map[int][2]interface{}
	sync.RWMutex
}

func (c *Contoller) Initialize() {
	logs.Info("Initialize Start")
	c.Data = make([]byte, c.Config.MaxLength)
	c.DataMap = make(map[int][2]interface{})
	c.ListenUdp()
	logs.Info("初始化成功，监听:", c.Config.Ip, ":", c.Config.Port)
}

func (c *Contoller) ListenUdp() {
	c.UdpSocket, c.Error = net.ListenUDP("udp4",
		&net.UDPAddr{IP: net.IPv4(c.Config.Ip[0], c.Config.Ip[1], c.Config.Ip[2], c.Config.Ip[3]), Port: c.Config.Port})
	if c.Error != nil {
		logs.Error("Error:", c.Error)
	}

}

func (c *Contoller) GetData() {
	udplen, udpaddr, err := c.UdpSocket.ReadFromUDP(c.Data)
	if err != nil {
		logs.Error("数据获取失败")
	}
	c.Config.AllLength = int32(udplen)
	logs.Info("报文长度", udplen, "报文源地址", udpaddr)
	logs.Info("Receive:", fmt.Sprintf("% 2x\n", c.Data[:udplen]))
	c.ParsingMessage(c.Data[:c.Config.AllLength])
}

func (c *Contoller) ParsingMessage(data []byte) {
	//head:=data[0:2]
	start := byteschange.BytesToInt16(data[2:4])
	length := byteschange.BytesToInt16(data[4:6])
	logs.Info("起始地址:", start, "数据段长度:", length)
	if length/8 >= 1 {
		var anyslice [2]interface{}
		for i := 6; i < int(c.Config.AllLength)-2; i += 8 {
			//val:=byteschange.ByteToFloat32(data[i:i+4])
			val := byteschange.BytesToInt32(data[i : i+4])
			times := byteschange.BytesToInt32(data[i+4 : i+8])
			anyslice[0], anyslice[1] = val, times
			c.Lock()
			c.DataMap[start] = anyslice
			start++
			c.Unlock()
		}
	} else {
		logs.Debug("Length not enough")
	}
}
