# driver-develop-go-demo

#### 介绍
第三方驱动开发go语言demo

快速开始
准备
配置和点表文件 data.db

流程

1.加载配置 2.加载测点 3.解析报文 4.写入实时库 5.重复 3和4
说明文档 http://192.168.20.10:8090/x/9A44
#### 软件架构
软件架构说明

1.目录结构
        //每个方法一个go文件
        各个方法.go
        //例子目录
        example
              //实现最简单的解析此数据，写实时值的程序
              simple

                    main.go
             //在简单的基础上增加控制功能的应用
              control

                    main.go
         //例句本项目提供了什么接口
          cmd

                main.go 

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
